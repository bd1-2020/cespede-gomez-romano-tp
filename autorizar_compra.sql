create or replace function autorizar_compra(n_tarjeta tarjeta.nrotarjeta%type,
						codigo tarjeta.codseguridad%type,
						n_comercio comercio.nrocomercio%type,
						monto_abonado compra.monto%type) returns boolean as $$
	declare
		tarjeta_encontrada record;  
		compras_pendientes_de_pago compra.monto%type;
		
	begin
		select * into tarjeta_encontrada from tarjeta t where n_tarjeta = t.nrotarjeta; 
		compras_pendientes_de_pago := (select sum (monto) from compra c where c.nrotarjeta = n_tarjeta and c.pagado = false);

		if not found  then           
			insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
				values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 'Tarjeta no valida');
						
			return false;	
		
		elsif tarjeta_encontrada.codseguridad != codigo then
			 insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
				values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 'Codigo de seguridad no valido');
					    
			 return false;

		elsif tarjeta_encontrada.estado = 'suspendida' then
			 insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
			 	values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 'La tarjeta se encuentra suspendida');
					    
			 return false;
			 
		elsif tarjeta_encontrada.estado = 'anulada' then
			insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
				values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 'Plazo de vigencia expirado');
					    
			return false;
						
		elsif tarjeta_encontrada.limitecompra < (compras_pendientes_de_pago + monto_abonado)  then
			insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
				values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 'Supera límite de tarjeta');
					    
			return false;
	
		else
			insert into compra (nrotarjeta, nrocomercio, fecha, monto, pagado)
				 values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, false);
		
			return true;
		
		end if;
	end;
$$ language plpgsql;
