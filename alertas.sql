create or replace function alertar_rechazos() returns trigger as $$
		
	begin
		insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
			values (new.nrotarjeta, current_timestamp, new.nrorechazo, 0, concat('Se ha rechazado su compra porque "', 					new.motivo, '"'));

		perform * from rechazo r where r.nrotarjeta = new.nrotarjeta
						and r.nrorechazo != new.nrorechazo
						and extract(day from r.fecha) = extract(day from new.fecha)
						and extract(month from r.fecha) = extract(month from new.fecha)
						and extract(year from r.fecha) = extract(year from new.fecha)
						and r.motivo = 'Supera límite de tarjeta'
						and new.motivo = 'Supera límite de tarjeta';
		if found then
			update tarjeta set estado = 'suspendida' where nrotarjeta = new.nrotarjeta;
			insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
				values (new.nrotarjeta, current_timestamp, new.nrorechazo, 32, 'Se ha suspendido la tarjeta preventivamente');
		end if;
		return new;
	end;
$$ language plpgsql;

create or replace function alertar_compras() returns trigger as $$
		
	begin  
		perform * from compra c where c.nrotarjeta = new.nrotarjeta
						and c.nrooperacion != new.nrooperacion
						and c.fecha >= new.fecha - (1 * interval '1 minute')
						and c.nrocomercio != new.nrocomercio
						and (select codigopostal from comercio where nrocomercio = c.nrocomercio) = (select 								codigopostal from comercio where nrocomercio = new.nrocomercio);
		if found then
			insert into alerta (nrotarjeta, fecha, nrooperacion, codalerta, descripcion)
				values (new.nrotarjeta, current_timestamp, new.nrooperacion, 1, 'Se han realizado dos compras al mismo tiempo en comercios diferentes');
		end if;

		perform * from compra c where c.nrotarjeta = new.nrotarjeta
						and c.nrooperacion != new.nrooperacion
						and c.fecha >= new.fecha - (5 * interval '1 minute')
						and (select codigopostal from comercio where nrocomercio = c.nrocomercio) != (select 								codigopostal from comercio where nrocomercio = new.nrocomercio);
		if found then
			insert into alerta (nrotarjeta, fecha, nrooperacion, codalerta, descripcion)
				values (new.nrotarjeta, current_timestamp, new.nrooperacion, 5, 'Se han realizado dos compras seguidas en comercios muy alejados');
		end if;
		return new;
	end;
$$ language plpgsql;

create trigger rechazos_tgr after insert on rechazo for each row execute procedure alertar_rechazos();

create trigger compras_tgr after insert on compra for each row execute procedure alertar_compras();
