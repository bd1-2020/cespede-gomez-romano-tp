= Bases De Datos 1: Trabajo Práctico
:title-page:
:numbered:
:source-highlighter: coderay
:tabsize: 4
Nicolas Cespede <cespede.alannicolas.42984@gmail.com>; Mario Gomez <gomezmariobmx@gmail.com>; Facundo Romano <romano.facundo@gmail.com>
v1, {4/6/2020}. Docentes Hernán Rondelli y Hernán Czemerinski (COM-01)


== Introducción

En el siguiente trabajo practico se deberá realizar, mediante un modelo de referencia, una base de datos que contenga las tablas necesarias para almacenar la información relativa a una tarjeta de crédito. Una vez creadas dichas tablas, se deberán incorporar las PK’s y FK’s de dichas tablas. Además, el usuario deberá tener la facultad de suprimirlas si desea hacerlo.
Dentro de la implementación, se incluirán los stored procedures, “autorización de compra” y “generación del resumen”, y el trigger “alertas a clientes”. En consecuencia, el código SQL se tendrá que ejecutar dentro de una aplicación escrita en Go.
En última estancia, se deberán guardar los datos pedidos en una base de datos NoSQL basada en JSON, en donde se utilizará la base de datos BoltDB.


== Descripción


Comenzamos el diseño del trabajo practico mediante el desarrollo de las tablas de la base de datos, junto con las FK’s y PK’s con su respectivo script para borrarlo en caso de que se desee. En primera instancia realizamos las tablas clientes, comercios y tarjetas en diferentes scripts y luego lo unificamos en uno por una optimización de diseño.
En la segunda etapa del desarrollo del trabajo, discutimos en grupo acerca de cómo podríamos realizar el diseño relacionado con las funciones y Go.
Luego de charlar en grupo, se llegó a la conclusión que los 3 items (autorización de compra, generación del resumen y alertas a clientes) son funciones que no van en Go, sino que Go las invoca.
Una vez sacada esa duda, nos repartimos las tareas para crear la base de datos, las tablas, las pks y fks (agregarlas y borrarlas), los comercios, las tarjetas y los clientes.

Respecto a la resolución de las funciones, primer habia una charla y un intento de realizar el codigo juntos, después cada uno trataba de aportar de forma individual hasta la próxima reunión. 
La primer función estuvo antes de la reunión, así que se subió al repositorio para que todos los integrantes la vean, luego se revisó y quedó aceptada. Se dejó además un test para esa función.
En relación a las otras dos funciones, se realizaron de la misma manera. Primero reunión grupal y luego individual. El primero que llegó a una solución compartió a los demás para una revisión.
Hay que aclarar que en la función alertas de clientes, nos llevó más tiempo entender que teníamos que hacer triggers dado que había una sucesión de eventos que tenían que ser en orden ( una vez que hay un RECHAZO, se crea una ALERTA). El hecho de que cada un minuto se tenga que activar el trigger nos confunde si iba en el trigger o en Go, pero luego de ver las diapositivas nos quedó claro que eso va en Go.
Para poder realizar la aplicación CLI en Go, tuvimos que agregar de forma manual en las funciones todo el contenido de los scripts que tienen el código en SQL, ya que en un principio pensábamos que dichos scripts se podían llamar desde la aplicación.

== Implementación

Se disponen de dos clientes de Go. El primero trabaja con una base de datos en  Postgres  y el  segundo con una base de datos utilizando Bolt db. 
Antes de empezar a trabajar en los dos clientes, se construyó por separado, la base de datos, las tablas , las creaciones de Pks y Fks, el borrado de las Pks y Fks, agregado de datos a las tablas ( cliente, tarjeta y comercio),  las funciones (autorización de compra, generación del resumen y alertas a clientes), y por último unos pequeños test que probaban las funciones individualmente. 
Una vez listos los puntos anteriores, se creó el  primer Cliente (*cliSQL.go*) con un pequeño menú para ejecutar los siguientes ítems:

*Creación de la base de datos y tablas*

[source,sql]
----
drop database if exists tarjetas;
create database tarjetas;

\c tarjetas

create table cliente(nrocliente	serial, nombre text, apellido text, domicilio text, telefono char(12));
create table tarjeta(nrotarjeta char(16), nrocliente int, validadesde char(6), validahasta char(6), codseguridad char(4), limitecompra decimal(8,2), estado char(10));
create table comercio(nrocomercio serial, nombre text, domicilio text, codigopostal char(8), telefono char(12));
create table compra(nrooperacion serial, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), pagado boolean);
create table rechazo(nrorechazo serial, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), motivo text);
create table cierre(anio int, mes int, terminacion int, fechainicio date, fechacierre date, fechavto date);	
create table cabecera(nroresumen serial, nombre text, apellido text, domicilio text, nrotarjeta char(16), desde date, hasta date, vence date, total decimal(7,2));
create table detalle(nroresumen int, nrolinea int, fecha date, nombrecomercio text, monto decimal(7,2));
create table alerta(nroalerta serial, nrotarjeta char(16), fecha timestamp, nrorechazo int, nrooperacion int, codalerta int, descripcion text);
create table consumo(nroconsumo serial, nrotarjeta char(16), codseguridad char(4), nrocomercio int, monto decimal(7,2));
----


*Creación de la Pks y Fks*
[source,sql]
----
alter table cliente add constraint cliente_pk primary key (nrocliente);
alter table tarjeta add constraint tarjeta_pk primary key (nrotarjeta);
alter table comercio add constraint comercio_pk primary key (nrocomercio);
alter table compra add constraint compra_pk primary key (nrooperacion);
alter table rechazo add constraint rechazo_pk primary key (nrorechazo);
alter table cierre add constraint cierre_pk primary key (anio,mes,terminacion);
alter table cabecera add constraint cabecera_pk primary key (nroresumen);
alter table detalle add constraint detalle_pk primary key (nroresumen, nrolinea);
alter table alerta add constraint alerta_pk primary key (nroalerta);
alter table consumo add constraint consumo_pk primary key (nroconsumo);

alter table tarjeta add constraint tarjeta_nrocliente_fk foreign key (nrocliente) references cliente (nrocliente);
alter table compra add constraint compra_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta (nrotarjeta);
alter table compra add constraint compra_nrocomercio_fk foreign key (nrocomercio) references comercio (nrocomercio);
alter table rechazo add constraint rechazo_nrocomercio_fk foreign key (nrocomercio) references comercio (nrocomercio);
alter table cabecera add constraint cabecera_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta (nrotarjeta);
alter table alerta add constraint alerta_nrorechazo_fk foreign key (nrorechazo) references rechazo (nrorechazo);
alter table alerta add constraint alerta_nrooperacion_fk foreign key (nrooperacion) references compra (nrooperacion);
----

*Borrado de las Pks y Fks*
[source,sql]
----
alter table tarjeta drop constraint tarjeta_nrocliente_fk;
alter table compra drop constraint compra_nrotarjeta_fk;
alter table compra drop constraint compra_nrocomercio_fk;
alter table rechazo drop constraint rechazo_nrocomercio_fk;
alter table cabecera drop constraint cabecera_nrotarjeta_fk;
alter table alerta drop constraint alerta_nrorechazo_fk;
alter table alerta drop constraint alerta_nrooperacion_fk;

alter table cliente drop constraint cliente_pk;
alter table tarjeta drop constraint tarjeta_pk;
alter table comercio drop constraint comercio_pk;
alter table compra drop constraint compra_pk;
alter table rechazo drop constraint rechazo_pk;
alter table cierre drop constraint cierre_pk;
alter table cabecera drop constraint cabecera_pk;
alter table detalle drop constraint detalle_pk;
alter table alerta drop constraint alerta_pk;
alter table consumo drop constraint consumo_pk;
----

*Carga de datos a las tablas*
[source,ruby]
Ejemplo tabla clientes

[source,sql]
----
-- CLIENTES

insert into cliente (nombre, apellido, domicilio, telefono) values ('Federico', 'Baez', 'Pardo 4232', '541134254352');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Hernan', 'Gomez', 'Paunero 432', '541155346637');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Jose', 'Alvaro', 'Correntes 132', '541167534637');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Marta', 'Rodriguez', 'Mendoza 223', '541188327321');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Facundo', 'Romano', 'Lala 123', '541198432903');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Flavia', 'Funes', 'Estrada 1450', '541176859304');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Micaela', 'Nedelco', 'San Jose 543', '541176584934');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Marcos', 'Peralta', 'Misiones 4202', '541188432173');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Nicolas', 'Zodape', 'Jujuy 443', '541112343564');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Charlotte', 'Harlot', 'Avenida Acacia 22', '541112343564');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Federico', 'Suarez', 'Granada 422', '541134243251');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Pedro', 'Gomez', 'Pingon 1232', '541163346137');
insert into cliente (nombre, apellido, domicilio, telefono) values ('James', 'Hetfiled', 'Inhell 666', '541167534637');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Gustavo', 'Domingo', 'Jungla 5634', '541154727321');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Jorge', 'Grivas', 'Anglo 2415', '541198437493');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Juan', 'Olaf', 'Hook Street 154', '541194859304');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Cliff', 'Burton', 'Heaven 5223', '541176584934');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Marcos', 'Fajin', 'Hito 462', '541185352173');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Julia', 'Manguera', 'Ringo 20', '541112353561');
insert into cliente (nombre, apellido, domicilio, telefono) values ('Angel', 'Mercado', 'Avenida Central 2322', '541112363264');
----

*Funcion - autorización_compra*

Llamaremos consumo a el momento antes de volverse una compra aceptada.
Esta función permite autorizar una compra. Al llamar a la función se le ingresan los datos número de tarjeta, código de seguridad, número de comercio y monto del consumo.  Los pasos para poder autorizar el consumo son los siguientes:
Busca que el número de tarjeta coincida con uno existente en la tabla _tarjeta_, en caso de que no exista, la tarjeta se considera inválida y no se toma el consumo.
En caso de coincidir con una de las tarjetas de la base de datos, se verifica que el código de seguridad coincida con el cargado. En caso de no coincidir, se descarta el consumo por código de seguridad invalido.
Una vez validada la clave de seguridad, se verifica que la tarjeta no esté suspendida o anulada. Si llega a ser este el caso, el consumo se descarta porque la tarjeta no es vigente.
Llegando al caso que la tarjeta pasó los controles anteriores,antes de poder cargar el consumo, se busca el límite de compra de la tarjeta y se calcula las compras que aún no fueron pagadas. Se corrobora que el consumo más lo acumulado en compras aún no pagadas no supere el límite de la tarjeta, recién entonces se puede cargar el consumo. Recién ahora consideramos que el consumo se volvió una compra válida, insertando los datos que se ingresan en la función en la tabla compras.
Notar que el estado de la compra es false porque está pendiente de pago.
[source,sql]
----
create or replace function autorizar_compra(n_tarjeta tarjeta.nrotarjeta%type,
						codigo tarjeta.codseguridad%type,
						n_comercio comercio.nrocomercio%type,
						monto_abonado compra.monto%type) returns boolean as $$
	declare
		tarjeta_encontrada record;  
		compras_pendientes_de_pago compra.monto%type;
		
	begin
		select * into tarjeta_encontrada from tarjeta t where n_tarjeta = t.nrotarjeta; 
		compras_pendientes_de_pago := (select sum (monto) from compra c where c.nrotarjeta = n_tarjeta and c.pagado = false);

		if not found  then           
			insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
				values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 'Tarjeta no valida');
						
			return false;	
		
		elsif tarjeta_encontrada.codseguridad != codigo then
			 insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
				values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 'Codigo de seguridad no valido');
					    
			 return false;

		elsif tarjeta_encontrada.estado = 'suspendida' then
			 insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
			 	values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 'La tarjeta se encuentra suspendida');
					    
			 return false;
			 
		elsif tarjeta_encontrada.estado = 'anulada' then
			insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
				values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 'Plazo de vigencia expirado');
					    
			return false;
						
		elsif tarjeta_encontrada.limitecompra < (compras_pendientes_de_pago + monto_abonado)  then
			insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
				values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 'Supera límite de tarjeta');
					    
			return false;
	
		else
			insert into compra (nrotarjeta, nrocomercio, fecha, monto, pagado)
				 values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, false);
		
			return true;
		
		end if;
	end;
$$ language plpgsql;
----


*Funcion - generar_resumen*

Esta función toma (número de cliente, año y mes) y llena una entrada de la tabla _cabecera_ , ademas de una o varias entradas en la tabla _detalle_ .
Primero valida que la tarjeta esté en la base de datos. Luego busca si el cliente tiene más de una tarjeta y para cada una de ellas 
realiza el siguiente proceso.
Se fija que según el periodo ingresado y la terminación de la tarjeta el inicio y fin del periodo a cerrar la tarjeta. 
Realiza una carga a la tabla _cabecera_ con los datos del cliente  y además tomamos el número de resumen de la entrada realizada en cabecera para poder ingresarla en cada una de las cargas en la tabla _detalle_ . A su vez, cada vez que se realiza una carga en la tabla _detalle_  se va sumando los pagos para dar un total y cada compra  cambia su estado de _pagado_ a true. 
Finalmente se actualiza la entrada en _cabecera_ antes mencionada agregando el total a pagar por el  _cliente_ en ese periodo .
[source,sql]
----
create or replace function generar_resumen(n_cliente cliente.nrocliente%type,
						anio_par int,
						mes_par int) returns void as $$
	declare
		cliente_encontrado record;
		compra_aux record;
		tarjeta_aux record;
		cierre_aux record;
		total_aux cabecera.total%type;
		nroresumen_aux cabecera.nroresumen%type;
		nombre_comercio comercio.nombre%type;
		cont int := 1;

	begin
		select * into cliente_encontrado from cliente where nrocliente = n_cliente;
		  if not found then
	      		  raise 'Cliente % no existe.', n_cliente;
  		  end if;
		
		for tarjeta_aux in select * from tarjeta where nrocliente = n_cliente loop

			total_aux := 0;
			select * into cierre_aux from cierre cie where cie.anio = anio_par and cie.mes = mes_par and cie.terminacion = substring(tarjeta_aux.nrotarjeta, 16, 1)::int;

			insert into cabecera(nombre, apellido, domicilio, nrotarjeta, desde, hasta, vence) 
					values (cliente_encontrado.nombre, cliente_encontrado.apellido, cliente_encontrado.domicilio, tarjeta_aux.nrotarjeta, cierre_aux.fechainicio, cierre_aux.fechacierre, cierre_aux.fechavto);

			select into nroresumen_aux nroresumen from cabecera where nrotarjeta = tarjeta_aux.nrotarjeta
									and desde = cierre_aux.fechainicio
									and hasta = cierre_aux.fechacierre;

			for compra_aux in select * from compra where nrotarjeta = tarjeta_aux.nrotarjeta 
								and fecha::date >= (cierre_aux.fechainicio)::date 
								and fecha::date <= (cierre_aux.fechacierre)::date
								and pagado = false loop
				
				nombre_comercio := (select nombre from comercio where nrocomercio = compra_aux.nrocomercio);
				insert into detalle values (nroresumen_aux, cont, compra_aux.fecha, nombre_comercio, compra_aux.monto);
				total_aux := total_aux + compra_aux.monto;
				cont := cont + 1;
				update compra set pagado = true where nrooperacion = compra_aux.nrooperacion;
		
			end loop;
			
			update cabecera set total = total_aux where nrotarjeta = tarjeta_aux.nrotarjeta
									and desde = cierre_aux.fechainicio
									and hasta = cierre_aux.fechacierre;
		end loop;
	end;
$$ language plpgsql;
----



*Triggers - Alertas*

Consta de dos triggers, alerta_rechazos y alerta_compras.

_alerta_rechazos_

Después de ingresar un rechazo, este se carga a la tabla alertas.
A continuación se revisa si hay dos  rechazos por exceso de límite en el mismo día, mismo mes y año. Luego la tarjeta cambia de estado a suspendida, seguida de generar una alerta asociada al cambio de estado.

_alerta_compras_

Cuando una tarjeta registra dos compras en un lapso menor de un minuto en comercios distintos ubicados en el mismo código postal, se inserta un a _alerta_.
Luego, si una tarjeta registra dos compras en un lapso menor de 5 minutos en comercios con diferentes códigos postales,  se inserta un a _alerta_.

[source,sql]
----
create or replace function alertar_rechazos() returns trigger as $$
		
	begin
		insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
			values (new.nrotarjeta, current_timestamp, new.nrorechazo, 0, concat('Se ha rechazado su compra porque "', new.motivo, '"'));

		perform * from rechazo r where r.nrotarjeta = new.nrotarjeta
						and r.nrorechazo != new.nrorechazo
						and extract(day from r.fecha) = extract(day from new.fecha)
						and extract(month from r.fecha) = extract(month from new.fecha)
						and extract(year from r.fecha) = extract(year from new.fecha)
						and r.motivo = 'Supera límite de tarjeta'
						and new.motivo = 'Supera límite de tarjeta';
		if found then
			update tarjeta set estado = 'suspendida' where nrotarjeta = new.nrotarjeta;
			insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
				values (new.nrotarjeta, current_timestamp, new.nrorechazo, 32, 'Se ha suspendido la tarjeta preventivamente');
		end if;
		return new;
	end;
$$ language plpgsql;

create or replace function alertar_compras() returns trigger as $$
		
	begin  
		perform * from compra c where c.nrotarjeta = new.nrotarjeta
						and c.nrooperacion != new.nrooperacion
						and c.fecha >= new.fecha - (1 * interval '1 minute')
						and c.nrocomercio != new.nrocomercio
						and (select codigopostal from comercio where nrocomercio = c.nrocomercio) = (select codigopostal from comercio where nrocomercio = new.nrocomercio);
		if found then
			insert into alerta (nrotarjeta, fecha, nrooperacion, codalerta, descripcion)
				values (new.nrotarjeta, current_timestamp, new.nrooperacion, 1, 'Se han realizado dos compras al mismo tiempo en comercios diferentes');
		end if;

		perform * from compra c where c.nrotarjeta = new.nrotarjeta
						and c.nrooperacion != new.nrooperacion
						and c.fecha >= new.fecha - (5 * interval '1 minute')
						and (select codigopostal from comercio where nrocomercio = c.nrocomercio) != (select codigopostal from comercio where nrocomercio = new.nrocomercio);
		if found then
			insert into alerta (nrotarjeta, fecha, nrooperacion, codalerta, descripcion)
				values (new.nrotarjeta, current_timestamp, new.nrooperacion, 5, 'Se han realizado dos compras seguidas en comercios muy alejados');
		end if;
		return new;
	end;
$$ language plpgsql;

create trigger rechazos_tgr after insert on rechazo for each row execute procedure alertar_rechazos();

create trigger compras_tgr after insert on compra for each row execute procedure alertar_compras();
----

*Cli.go* _Cada minuto se trata de detectar una alerta_

 Se muestra esta sección porque lo demás ya se mostro.

[source,sql]
----
func detectarAlertas() {
	for opcion != 9 {
		var alertasAux int = 0
		proximaVez := time.Now().Truncate(time.Minute)
		proximaVez = proximaVez.Add(time.Minute)
		row, err := db.Query(`select count(*) from alerta;`)
		if err != nil {
			log.Fatal(err)
		}
		if row.Next() {
			if err = row.Scan(&alertasAux); err != nil {
				log.Fatal(err)
			}
		}
		if err != nil {
			log.Fatal(err)
		}
		if alertas < alertasAux {
			alertas = alertasAux
			fmt.Print("\n\n", separator, "\n\n SE HA DETECTADO UNA NUEVA ALERTA\n\n", separator, "\n\n")
		}
		time.Sleep(60 * time.Second)
	}
}
----


En cuanto al segundo Cliente (*cliNoSQL.go*), este muestra un menú que ofrece las opciones (crear la base de datos, insertar los datos de clientes, tarjetas, comercios y compras).
Lo que aportó Bolt y que utilizamos es  la conección a la base de datos,  la función de escritura de bucket y la sentencia para Marshaling los datos de clientes, tarjetas, comercios y compras. Lo demás es similar al archivo *cli.go*.

*Conexión a la base de datos*

[source,sql]
----
db, err = bolt.Open("tarjetas.db", 0600, nil)
			if err != nil {
				log.Fatal(err)
			}
			defer db.Close()
			fmt.Print("\n CREACION EXITOSA\n\n", separator)
----


*Insertar datos de clientes usando _escritura de Bucket y Marshaling_*

[source,sql]
----
func insertClientes() {
	for _, cliente := range clientes {
		data, err := json.Marshal(cliente)
		if err != nil {
			log.Fatal(err)
		}
		createUpdate(db, "cliente", []byte(strconv.Itoa(cliente.NroCliente)), data)
		resultado, err := readUnique(db, "cliente", []byte(strconv.Itoa(cliente.NroCliente)))
		fmt.Printf("\n%s\n", resultado)
	}
	fmt.Print("\n INSERCION DE DATOS DE CLIENTES EXITOSA\n\n")
	fmt.Printf("\n%s", separator)
}
----


== Conclusiones

Este trabajo nos llevó a ampliar nuestro conocimiento en la temática establecida para el mismo, fortaleciendo así, el uso y la ejecución de diferentes herramientas, lenguajes y tecnologías. Vamos incorporando dichos conocimientos para poder aplicarlos dentro del nivel académico y también en el marco laboral, siendo así fundamental para nuestra formación disciplinaria.
Desde nuestra experiencia personal, es la primera vez que trabajamos es estas circunstancias, lo cual nos llevó, en algunas ocasiones, a una ardua búsqueda de información por fuera del material dado, para poder llevar a cabo la realización del trabajo.
En síntesis, una vez concluimos con dicho trabajo, nos ofrecido una sensación gratificante ya que nos sentimos más capacitados para poder enfrentar proyectos de índole características.

