create or replace function generar_resumen(n_cliente cliente.nrocliente%type,
						anio_par int,
						mes_par int) returns void as $$
	declare
		cliente_encontrado record;
		compra_aux record;
		tarjeta_aux record;
		cierre_aux record;
		total_aux cabecera.total%type;
		nroresumen_aux cabecera.nroresumen%type;
		nombre_comercio comercio.nombre%type;
		cont int := 1;

	begin
		select * into cliente_encontrado from cliente where nrocliente = n_cliente;
		  if not found then
	      		  raise 'Cliente % no existe.', n_cliente;
  		  end if;
		
		for tarjeta_aux in select * from tarjeta where nrocliente = n_cliente loop

			total_aux := 0;
			select * into cierre_aux from cierre cie where cie.anio = anio_par and cie.mes = mes_par and cie.terminacion = 									substring(tarjeta_aux.nrotarjeta, 16, 1)::int;

			insert into cabecera(nombre, apellido, domicilio, nrotarjeta, desde, hasta, vence) 
					values (cliente_encontrado.nombre, cliente_encontrado.apellido, cliente_encontrado.domicilio, 							tarjeta_aux.nrotarjeta, cierre_aux.fechainicio, cierre_aux.fechacierre, 							cierre_aux.fechavto);

			select into nroresumen_aux nroresumen from cabecera where nrotarjeta = tarjeta_aux.nrotarjeta
									and desde = cierre_aux.fechainicio
									and hasta = cierre_aux.fechacierre;

			for compra_aux in select * from compra where nrotarjeta = tarjeta_aux.nrotarjeta 
								and fecha::date >= (cierre_aux.fechainicio)::date 
								and fecha::date <= (cierre_aux.fechacierre)::date
								and pagado = false loop
				
				nombre_comercio := (select nombre from comercio where nrocomercio = compra_aux.nrocomercio);
				insert into detalle values (nroresumen_aux, cont, compra_aux.fecha, nombre_comercio, compra_aux.monto);
				total_aux := total_aux + compra_aux.monto;
				cont := cont + 1;
				update compra set pagado = true where nrooperacion = compra_aux.nrooperacion;
		
			end loop;
			
			update cabecera set total = total_aux where nrotarjeta = tarjeta_aux.nrotarjeta
									and desde = cierre_aux.fechainicio
									and hasta = cierre_aux.fechacierre;
		end loop;
	end;
$$ language plpgsql;
