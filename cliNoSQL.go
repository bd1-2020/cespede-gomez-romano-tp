package main

import (
	"encoding/json"
	"fmt"
	bolt "github.com/coreos/bbolt"
	"log"
	"strconv"
)

type Cliente struct {
	NroCliente int
	Nombre     string
	Apellido   string
	Domicilio  string
	Telefono   string
}

type Tarjeta struct {
	NroTarjeta   string
	NroCliente   int
	ValidaDesde  string
	ValidaHasta  string
	CodSeguridad int
	LimiteCompra float64
	Estado       string
}

type Comercio struct {
	NroComercio  int
	Nombre       string
	Domicilio    string
	CodigoPostal int
	Telefono     string
}

type Compra struct {
	NroOperacion int
	NroTarjeta   string
	NroComercio  int
	Fecha        string
	Monto        float64
	Pagado       bool
}

const separator string = "////////////////////////////////////////////////////////////////////////////////\n"

var db *bolt.DB
var err error
var opcion int = 0
var clientes []Cliente
var tarjetas []Tarjeta
var comercios []Comercio
var compras []Compra

func main() {
	fillArrays()

	for opcion != 6 {

		fmt.Print("\nOpciones que desea realizar\n\n")
		fmt.Print("1 = Crear base de datos Tarjetas\n")
		fmt.Print("2 = Insertar datos de los clientes\n")
		fmt.Print("3 = Insertar datos de las tarjetas\n")
		fmt.Print("4 = Insertar datos de los comercios\n")
		fmt.Print("5 = Insertar datos de las compras\n")
		fmt.Print("6 = Terminar y salir\n")

		fmt.Print("\nElija una opcion: ")
		fmt.Scanf("%d", &opcion)
		fmt.Print("opcion seleccionada: ", opcion)
		fmt.Print("\n")

		if opcion == 1 {
			db, err = bolt.Open("tarjetas.db", 0600, nil)
			if err != nil {
				log.Fatal(err)
			}
			defer db.Close()
			fmt.Print("\n CREACION EXITOSA\n\n", separator)
		}

		if opcion == 2 {
			insertClientes()
		}

		if opcion == 3 {
			insertTarjetas()
		}

		if opcion == 4 {
			insertComercios()
		}

		if opcion == 5 {
			insertCompras()
		}

		if opcion == 6 {
			fmt.Print("\n SALIENDO...\n")
		}
	}
}

func fillArrays() {
	clientes = append(clientes, Cliente{1, "Federico", "Baez", "Pardo 4232", "541134254352"})
	clientes = append(clientes, Cliente{2, "Hernan", "Gomez", "Paunero 432", "541155346637"})
	clientes = append(clientes, Cliente{3, "Jose", "Alvaro", "Correntes 132", "541167534637"})
	tarjetas = append(tarjetas, Tarjeta{"8439201934832810", 1, "201806", "202006", 863, 100000.0, "vigente"})
	tarjetas = append(tarjetas, Tarjeta{"3948201948503941", 2, "202104", "202304", 634, 75000.0, "vigente"})
	tarjetas = append(tarjetas, Tarjeta{"1039482830482950", 3, "201901", "202101", 7346, 500000.0, "vigente"})
	comercios = append(comercios, Comercio{1, "Mercería Dany", "Cervantes 456", 1744, "481-1212"})
	comercios = append(comercios, Comercio{2, "Mercería Nico", "Perón 125", 1655, "481-1226"})
	comercios = append(comercios, Comercio{3, "Mercería Eva", "Colon 784", 1943, "481-4578"})
	compras = append(compras, Compra{1, "8439201934832810", 863, "2020-05-16 21:39:45", 4000.0, false})
	compras = append(compras, Compra{2, "1039482830482950", 7346, "2020-05-25 15:23:34", 24000.0, false})
	compras = append(compras, Compra{3, "3948201948503941", 634, "2020-06-01 17:35:26", 7000.0, false})
}

func insertClientes() {
	for _, cliente := range clientes {
		data, err := json.Marshal(cliente)
		if err != nil {
			log.Fatal(err)
		}
		createUpdate(db, "cliente", []byte(strconv.Itoa(cliente.NroCliente)), data)
		resultado, err := readUnique(db, "cliente", []byte(strconv.Itoa(cliente.NroCliente)))
		fmt.Printf("\n%s\n", resultado)
	}
	fmt.Print("\n INSERCION DE DATOS DE CLIENTES EXITOSA\n\n")
	fmt.Printf("\n%s", separator)
}

func insertTarjetas() {
	for _, tarjeta := range tarjetas {
		data, err := json.Marshal(tarjeta)
		if err != nil {
			log.Fatal(err)
		}
		createUpdate(db, "tarjeta", []byte(tarjeta.NroTarjeta), data)
		resultado, err := readUnique(db, "tarjeta", []byte(tarjeta.NroTarjeta))
		fmt.Printf("\n%s\n", resultado)
	}
	fmt.Print("\n \n INSERCION DE DATOS DE TARJETAS EXITOSA\n\n")
	fmt.Printf("\n%s", separator)
}

func insertComercios() {
	for _, comercio := range comercios {
		data, err := json.Marshal(comercio)
		if err != nil {
			log.Fatal(err)
		}
		createUpdate(db, "comercio", []byte(strconv.Itoa(comercio.NroComercio)), data)
		resultado, err := readUnique(db, "comercio", []byte(strconv.Itoa(comercio.NroComercio)))
		fmt.Printf("\n%s\n", resultado)
	}
	fmt.Print("\n \n INSERCION DE DATOS DE COMERCIOS EXITOSA\n\n")
	fmt.Printf("\n%s", separator)
}

func insertCompras() {
	for _, compra := range compras {
		data, err := json.Marshal(compra)
		if err != nil {
			log.Fatal(err)
		}
		createUpdate(db, "compra", []byte(strconv.Itoa(compra.NroOperacion)), data)
		resultado, err := readUnique(db, "compra", []byte(strconv.Itoa(compra.NroOperacion)))
		fmt.Printf("\n%s\n", resultado)
	}
	fmt.Print("\n \n INSERCION DE DATOS DE COMPRAS EXITOSA\n\n")
	fmt.Printf("\n%s", separator)
}

func createUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()
	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))
	err = b.Put(key, val)
	if err != nil {
		return err
	}
	if err := tx.Commit(); err != nil {
		return err
	}
	return nil
}

func readUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})
	return buf, err
}
