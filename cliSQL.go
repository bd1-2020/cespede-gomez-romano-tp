package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"time"
)

type consumo struct {
	nroconsumo   int
	nrotarjeta   string
	codseguridad string
	nrocomercio  int
	monto        float64
}

const dbInfo string = "user=postgres password=123 host=localhost dbname=tarjetas sslmode=disable"
const separator string = "////////////////////////////////////////////////////////////////////////////////"
const cantConsumos int = 16

var db *sql.DB
var err error
var nroConsumo int = 1
var alertas int = 0
var opcion int = 0

func main() {
	for opcion != 9 {

		fmt.Print("\nOpciones que desea realizar\n\n")
		fmt.Print("1 = Crear base de datos Tarjetas\n")
		fmt.Print("2 = Crear tablas \n")
		fmt.Print("3 = Crear PKs y FKs\n")
		fmt.Print("4 = Borrar PKs y FKs\n")
		fmt.Print("5 = Cargar datos en las tablas\n")
		fmt.Print("6 = Crear y cargar funciones\n")
		fmt.Print("7 = Autorizar una compra (16 compras para autorizar)\n")
		fmt.Print("8 = Generar resumenes\n")
		fmt.Print("9 = Terminar y salir\n")

		fmt.Print("\nElija una opcion: ")
		fmt.Scanf("%d", &opcion)
		fmt.Print("opcion seleccionada: ", opcion)
		fmt.Print("\n")

		if opcion == 1 {
			createDatabase()
			db, err = sql.Open("postgres", dbInfo)
			if err != nil {
				log.Fatal(err)
			}
			defer db.Close()
		}

		if opcion == 2 {
			createTables()
		}

		if opcion == 3 {
			createPKsAndFKs()
		}

		if opcion == 4 {
			dropPKsAndFKs()
		}

		if opcion == 5 {
			insertValues()
		}

		if opcion == 6 {
			createFunction1()
			createFunction2()
			createFunction3()
			go detectarAlertas()
		}

		if opcion == 7 {
			autorizarCompra()
		}

		if opcion == 8 {
			generarResumenes()
		}

		if opcion == 9 {
			fmt.Print("\n SALIENDO...\n")
		}
	}
}

func createDatabase() {
	db, err = sql.Open("postgres", "user=postgres password=123 host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec("drop database if exists tarjetas")
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec("create database tarjetas")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("\n CREACION EXITOSA\n\n", separator, "\n")
}

func createTables() {
	_, err = db.Exec(`create table cliente(nrocliente	serial, nombre text, apellido text, domicilio text, telefono char(12));
			create table tarjeta(nrotarjeta char(16), nrocliente int, validadesde char(6), validahasta char(6), codseguridad 					char(4), limitecompra decimal(8,2), estado char(10));
			create table comercio(nrocomercio serial, nombre text, domicilio text, codigopostal char(8), telefono char(12));
			create table compra(nrooperacion serial, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), 					pagado boolean);
			create table rechazo(nrorechazo serial, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), 					motivo text);
			create table cierre(anio int, mes int, terminacion int, fechainicio date, fechacierre date, fechavto date);	
			create table cabecera(nroresumen serial, nombre text, apellido text, domicilio text, nrotarjeta char(16), desde 				date, hasta date, vence date, total decimal(7,2));
			create table detalle(nroresumen int, nrolinea int, fecha date, nombrecomercio text, monto decimal(7,2));
			create table alerta(nroalerta serial, nrotarjeta char(16), fecha timestamp, nrorechazo int, nrooperacion int, 					codalerta int, descripcion text);
			create table consumo(nroconsumo serial, nrotarjeta char(16), codseguridad char(4), nrocomercio int, monto 					decimal(7,2));`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("\n SE CREARON LAS TABLAS\n\n", separator, "\n")
}

func createPKsAndFKs() {
	_, err = db.Exec(`alter table cliente add constraint cliente_pk primary key (nrocliente);
			alter table tarjeta add constraint tarjeta_pk primary key (nrotarjeta);
			alter table comercio add constraint comercio_pk primary key (nrocomercio);
			alter table compra add constraint compra_pk primary key (nrooperacion);
			alter table rechazo add constraint rechazo_pk primary key (nrorechazo);
			alter table cierre add constraint cierre_pk primary key (anio,mes,terminacion);
			alter table cabecera add constraint cabecera_pk primary key (nroresumen);
			alter table detalle add constraint detalle_pk primary key (nroresumen, nrolinea);
			alter table alerta add constraint alerta_pk primary key (nroalerta);
			alter table consumo add constraint consumo_pk primary key (nroconsumo);
			alter table tarjeta add constraint tarjeta_nrocliente_fk foreign key (nrocliente) references cliente (nrocliente);
			alter table compra add constraint compra_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta (nrotarjeta);
			alter table compra add constraint compra_nrocomercio_fk foreign key (nrocomercio) references comercio (nrocomercio);
			alter table rechazo add constraint rechazo_nrocomercio_fk foreign key (nrocomercio) references comercio 				(nrocomercio);
			alter table cabecera add constraint cabecera_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta (nrotarjeta);
			alter table alerta add constraint alerta_nrorechazo_fk foreign key (nrorechazo) references rechazo (nrorechazo);
			alter table alerta add constraint alerta_nrooperacion_fk foreign key (nrooperacion) references compra 					(nrooperacion);`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("\n CREACION DE PKS Y FKS EXITOSA\n\n", separator, "\n")
}

func dropPKsAndFKs() {
	_, err = db.Exec(`alter table tarjeta drop constraint tarjeta_nrocliente_fk;
			alter table compra drop constraint compra_nrotarjeta_fk;
			alter table compra drop constraint compra_nrocomercio_fk;
			alter table rechazo drop constraint rechazo_nrocomercio_fk;
			alter table cabecera drop constraint cabecera_nrotarjeta_fk;
			alter table alerta drop constraint alerta_nrorechazo_fk;
			alter table alerta drop constraint alerta_nrooperacion_fk;
			alter table cliente drop constraint cliente_pk;
			alter table tarjeta drop constraint tarjeta_pk;
			alter table comercio drop constraint comercio_pk;
			alter table compra drop constraint compra_pk;
			alter table rechazo drop constraint rechazo_pk;
			alter table cierre drop constraint cierre_pk;
			alter table cabecera drop constraint cabecera_pk;
			alter table detalle drop constraint detalle_pk;
			alter table alerta drop constraint alerta_pk;
			alter table consumo drop constraint consumo_pk;`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("\n ELIMINACION DE PKS Y FKS EXITOSA\n\n", separator, "\n")
}

func insertValues() {
	_, err = db.Exec(`insert into cliente (nombre, apellido, domicilio, telefono) values ('Federico', 'Baez', 'Pardo 4232', 				'541134254352');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Hernan', 'Gomez', 'Paunero 432', 					'541155346637');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Jose', 'Alvaro', 'Correntes 132', 					'541167534637');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Marta', 'Rodriguez', 'Mendoza 223', 					'541188327321');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Facundo', 'Romano', 'Lala 123', 					'541198432903');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Flavia', 'Funes', 'Estrada 1450', 					'541176859304');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Micaela', 'Nedelco', 'San Jose 543', 					'541176584934');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Marcos', 'Peralta', 'Misiones 4202', 					'541188432173');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Nicolas', 'Zodape', 'Jujuy 443', 					'541112343564');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Charlotte', 'Harlot', 'Avenida Acacia 22', 					'541112343564');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Federico', 'Suarez', 'Granada 422', 					'541134243251');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Pedro', 'Gomez', 'Pingon 1232', 					'541163346137');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('James', 'Hetfiled', 'Inhell 666', 					'541167534637');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Gustavo', 'Domingo', 'Jungla 5634', 					'541154727321');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Jorge', 'Grivas', 'Anglo 2415', 					'541198437493');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Juan', 'Olaf', 'Hook Street 154', 					'541194859304');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Cliff', 'Burton', 'Heaven 5223', 					'541176584934');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Marcos', 'Fajin', 'Hito 462', '541185352173');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Julia', 'Manguera', 'Ringo 20', 					'541112353561');
			insert into cliente (nombre, apellido, domicilio, telefono) values ('Angel', 'Mercado', 'Avenida Central 2322', 					'541112363264');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Mercería Dany', 						'Cervantes 456','1744','481-1212');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Mercería Nico', 						'Perón 125','1655','481-1226');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Mercería Eva', 					'Colon 784','1943','481-4578');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Panadería Julia', 
					'Perón 1655','1744','481-1231');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Panadería Anarquista',
					'Justo 100','1744','481-9989');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Panadería Lucas', 						'Melo 584','1103','481-7854');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Panadería Figaza', 
					'Roca 125','1744','481-1254');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Panadería Rosca', 
					'Junin 894','11943','481-1589');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Carnicería Pancho', 						'Echeverria 1364','1432','481-5862');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Carnicería El Morci', 
					'Pascal 584','1432','481-2654');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Carnicería La vaca',
					'Perón 320','1744','481-6569');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Carnicería El Toro',
					'Mendoza 584','1655','481-6269');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Carnicería La vaca Loca', 
					'Junin 120','1103','481-6339');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Verdulería Banana Loca',
					'Perón 565','1744','481-6325');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Verdulería Tomate Punk',
					'Carriego 568','1744','481-1748');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Verdulería Morron Rojo',
					'Eva Perón 101','1655','481-7852');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Verdulería Tu vedulero ',
					'Melo 154','1864','481-5236');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Verdulería Hay Repollo',
					'Hernandarías 256','1274','481-6632');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Verdulería Marcelo',
					'Perón 1010','1935','481-6256');
			insert into comercio (nombre, domicilio, codigopostal, telefono) values('Verdulería Cosmo',
					'Alcorta 2525','1744','481-3652');
			insert into tarjeta values('8439201934832810', 1, '201806', '202006', '863', '100000', 'vigente');
			insert into tarjeta values('3948201948503941', 2, '202104', '202304', '634', '75000', 'vigente');
			insert into tarjeta values('1039482830482950', 3, '201901', '202101', '7346', '500000', 'vigente');
			insert into tarjeta values('8472691728478372', 4, '201011', '201211', '363', '20000', 'suspendida');
			insert into tarjeta values('8372071037307204', 5, '201010', '201210', '334', '150000', 'anulada');
			insert into tarjeta values('2284746861618333', 6, '200906', '201106', '654', '60000', 'anulada');
			insert into tarjeta values('1282730827281717', 7, '201609', '201809', '185', '25000', 'anulada');
			insert into tarjeta values('1827471028275236', 8, '201406', '201606', '753', '90000', 'suspendida');
			insert into tarjeta values('7482730185133523', 9, '201202', '201402', '634', '100000', 'suspendida');
			insert into tarjeta values('2346245345234256', 10,'200702', '200902', '643', '25000', 'anulada');
			insert into tarjeta values('6346245257547322', 11,'200805', '201105', '8656', '250000', 'anulada');
			insert into tarjeta values('3462136436346252', 11,'200903', '201103', '732', '30000', 'anulada');
			insert into tarjeta values('2462462543653642', 12,'201711', '202011', '645', '60000', 'vigente');
			insert into tarjeta values('2645753545262637', 13,'201910', '202310', '673', '95000', 'suspendida');
			insert into tarjeta values('4624624627747555', 14,'201109', '201509', '752', '15000', 'suspendida');
			insert into tarjeta values('4373724624635754', 15,'202206', '202406', '963', '50000', 'vigente');
			insert into tarjeta values('2352464363755473', 15,'201208', '201608', '8123', '300000', 'anulada');
			insert into tarjeta values('3463246427537532', 16,'200706', '200906', '235', '70000', 'anulada');
			insert into tarjeta values('5724645748753346', 17,'201102', '201902', '644', '100000', 'suspendida');
			insert into tarjeta values('8645735753645746', 18,'200512', '201012', '854', '123000', 'suspendida');
			insert into tarjeta values('1879876542345653', 19,'200302', '200802', '745', '80000', 'anulada');
			insert into tarjeta values('6734347778733455', 20,'201712', '202112', '245', '40000', 'suspendida');
			insert into cierre values(2020, 1, 0, '2020-01-01', '2020-01-31', '2020-02-15');
			insert into cierre values(2020, 2, 0, '2020-02-01', '2020-02-29', '2020-03-15');
			insert into cierre values(2020, 3, 0, '2020-03-01', '2020-03-31', '2020-04-15');
			insert into cierre values(2020, 4, 0, '2020-04-01', '2020-04-30', '2020-05-15');
			insert into cierre values(2020, 5, 0, '2020-05-01', '2020-05-31', '2020-06-15');
			insert into cierre values(2020, 6, 0, '2020-06-01', '2020-06-30', '2020-07-15');
			insert into cierre values(2020, 7, 0, '2020-07-01', '2020-07-31', '2020-08-15');
			insert into cierre values(2020, 8, 0, '2020-08-01', '2020-08-31', '2020-09-15');
			insert into cierre values(2020, 9, 0, '2020-09-01', '2020-09-30', '2020-10-15');
			insert into cierre values(2020, 10, 0, '2020-10-01', '2020-10-31', '2020-11-15');
			insert into cierre values(2020, 11, 0, '2020-11-01', '2020-11-30', '2020-12-15');
			insert into cierre values(2020, 12, 0, '2020-12-01', '2020-12-31', '2021-01-15');
			insert into cierre values(2020, 1, 1, '2020-01-02', '2020-02-01', '2020-02-16');
			insert into cierre values(2020, 2, 1, '2020-02-02', '2020-03-01', '2020-03-16');
			insert into cierre values(2020, 3, 1, '2020-03-02', '2020-04-01', '2020-04-16');
			insert into cierre values(2020, 4, 1, '2020-04-02', '2020-05-01', '2020-05-16');
			insert into cierre values(2020, 5, 1, '2020-05-02', '2020-06-01', '2020-06-16');
			insert into cierre values(2020, 6, 1, '2020-06-02', '2020-07-01', '2020-07-16');
			insert into cierre values(2020, 7, 1, '2020-07-02', '2020-08-01', '2020-08-16');
			insert into cierre values(2020, 8, 1, '2020-08-02', '2020-09-01', '2020-09-16');
			insert into cierre values(2020, 9, 1, '2020-09-02', '2020-10-01', '2020-10-16');
			insert into cierre values(2020, 10, 1, '2020-10-02', '2020-11-01', '2020-11-16');
			insert into cierre values(2020, 11, 1, '2020-11-02', '2020-12-01', '2020-12-16');
			insert into cierre values(2020, 12, 1, '2020-12-02', '2021-01-01', '2021-01-16');
			insert into cierre values(2020, 1, 2, '2020-01-03', '2020-02-02', '2020-02-17');
			insert into cierre values(2020, 2, 2, '2020-02-03', '2020-03-02', '2020-03-17');
			insert into cierre values(2020, 3, 2, '2020-03-03', '2020-04-02', '2020-04-17');
			insert into cierre values(2020, 4, 2, '2020-04-03', '2020-05-02', '2020-05-17');
			insert into cierre values(2020, 5, 2, '2020-05-03', '2020-06-02', '2020-06-17');
			insert into cierre values(2020, 6, 2, '2020-06-03', '2020-07-02', '2020-07-17');
			insert into cierre values(2020, 7, 2, '2020-07-03', '2020-08-02', '2020-08-17');
			insert into cierre values(2020, 8, 2, '2020-08-03', '2020-09-02', '2020-09-17');
			insert into cierre values(2020, 9, 2, '2020-09-03', '2020-10-02', '2020-10-17');
			insert into cierre values(2020, 10, 2, '2020-10-03', '2020-11-02', '2020-11-17');
			insert into cierre values(2020, 11, 2, '2020-11-03', '2020-12-02', '2020-12-17');
			insert into cierre values(2020, 12, 2, '2020-12-03', '2021-01-02', '2021-01-17');
			insert into cierre values(2020, 1, 3, '2020-01-04', '2020-02-03', '2020-02-18');
			insert into cierre values(2020, 2, 3, '2020-02-04', '2020-03-03', '2020-03-18');
			insert into cierre values(2020, 3, 3, '2020-03-04', '2020-04-03', '2020-04-18');
			insert into cierre values(2020, 4, 3, '2020-04-04', '2020-05-03', '2020-05-18');
			insert into cierre values(2020, 5, 3, '2020-05-04', '2020-06-03', '2020-06-18');
			insert into cierre values(2020, 6, 3, '2020-06-04', '2020-07-03', '2020-07-18');
			insert into cierre values(2020, 7, 3, '2020-07-04', '2020-08-03', '2020-08-18');
			insert into cierre values(2020, 8, 3, '2020-08-04', '2020-09-03', '2020-09-18');
			insert into cierre values(2020, 9, 3, '2020-09-04', '2020-10-03', '2020-10-18');
			insert into cierre values(2020, 10, 3, '2020-10-04', '2020-11-03', '2020-11-18');
			insert into cierre values(2020, 11, 3, '2020-11-04', '2020-12-03', '2020-12-18');
			insert into cierre values(2020, 12, 3, '2020-12-04', '2021-01-03', '2021-01-18');
			insert into cierre values(2020, 1, 4, '2020-01-05', '2020-02-04', '2020-02-19');
			insert into cierre values(2020, 2, 4, '2020-02-05', '2020-03-04', '2020-03-19');
			insert into cierre values(2020, 3, 4, '2020-03-05', '2020-04-04', '2020-04-19');
			insert into cierre values(2020, 4, 4, '2020-04-05', '2020-05-04', '2020-05-19');
			insert into cierre values(2020, 5, 4, '2020-05-05', '2020-06-04', '2020-06-19');
			insert into cierre values(2020, 6, 4, '2020-06-05', '2020-07-04', '2020-07-19');
			insert into cierre values(2020, 7, 4, '2020-07-05', '2020-08-04', '2020-08-19');
			insert into cierre values(2020, 8, 4, '2020-08-05', '2020-09-04', '2020-09-19');
			insert into cierre values(2020, 9, 4, '2020-09-05', '2020-10-04', '2020-10-19');
			insert into cierre values(2020, 10, 4, '2020-10-05', '2020-11-04', '2020-11-19');
			insert into cierre values(2020, 11, 4, '2020-11-05', '2020-12-04', '2020-12-19');
			insert into cierre values(2020, 12, 4, '2020-12-05', '2021-01-04', '2021-01-19');
			insert into cierre values(2020, 1, 5, '2020-01-06', '2020-02-05', '2020-02-20');
			insert into cierre values(2020, 2, 5, '2020-02-06', '2020-03-05', '2020-03-20');
			insert into cierre values(2020, 3, 5, '2020-03-06', '2020-04-05', '2020-04-20');
			insert into cierre values(2020, 4, 5, '2020-04-06', '2020-05-05', '2020-05-20');
			insert into cierre values(2020, 5, 5, '2020-05-06', '2020-06-05', '2020-06-20');
			insert into cierre values(2020, 6, 5, '2020-06-06', '2020-07-05', '2020-07-20');
			insert into cierre values(2020, 7, 5, '2020-07-06', '2020-08-05', '2020-08-20');
			insert into cierre values(2020, 8, 5, '2020-08-06', '2020-09-05', '2020-09-20');
			insert into cierre values(2020, 9, 5, '2020-09-06', '2020-10-05', '2020-10-20');
			insert into cierre values(2020, 10, 5, '2020-10-06', '2020-11-05', '2020-11-20');
			insert into cierre values(2020, 11, 5, '2020-11-06', '2020-12-05', '2020-12-20');
			insert into cierre values(2020, 12, 5, '2020-12-06', '2021-01-05', '2021-01-20');
			insert into cierre values(2020, 1, 6, '2020-01-07', '2020-02-06', '2020-02-21');
			insert into cierre values(2020, 2, 6, '2020-02-07', '2020-03-06', '2020-03-21');
			insert into cierre values(2020, 3, 6, '2020-03-07', '2020-04-06', '2020-04-21');
			insert into cierre values(2020, 4, 6, '2020-04-07', '2020-05-06', '2020-05-21');
			insert into cierre values(2020, 5, 6, '2020-05-07', '2020-06-06', '2020-06-21');
			insert into cierre values(2020, 6, 6, '2020-06-07', '2020-07-06', '2020-07-21');
			insert into cierre values(2020, 7, 6, '2020-07-07', '2020-08-06', '2020-08-21');
			insert into cierre values(2020, 8, 6, '2020-08-07', '2020-09-06', '2020-09-21');
			insert into cierre values(2020, 9, 6, '2020-09-07', '2020-10-06', '2020-10-21');
			insert into cierre values(2020, 10, 6, '2020-10-07', '2020-11-06', '2020-11-21');
			insert into cierre values(2020, 11, 6, '2020-11-07', '2020-12-06', '2020-12-21');
			insert into cierre values(2020, 12, 6, '2020-12-07', '2021-01-06', '2021-01-21');
			insert into cierre values(2020, 1, 7, '2020-01-08', '2020-02-07', '2020-02-22');
			insert into cierre values(2020, 2, 7, '2020-02-08', '2020-03-07', '2020-03-22');
			insert into cierre values(2020, 3, 7, '2020-03-08', '2020-04-07', '2020-04-22');
			insert into cierre values(2020, 4, 7, '2020-04-08', '2020-05-07', '2020-05-22');
			insert into cierre values(2020, 5, 7, '2020-05-08', '2020-06-07', '2020-06-22');
			insert into cierre values(2020, 6, 7, '2020-06-08', '2020-07-07', '2020-07-22');
			insert into cierre values(2020, 7, 7, '2020-07-08', '2020-08-07', '2020-08-22');
			insert into cierre values(2020, 8, 7, '2020-08-08', '2020-09-07', '2020-09-22');
			insert into cierre values(2020, 9, 7, '2020-09-08', '2020-10-07', '2020-10-22');
			insert into cierre values(2020, 10, 7, '2020-10-08', '2020-11-07', '2020-11-22');
			insert into cierre values(2020, 11, 7, '2020-11-08', '2020-12-07', '2020-12-22');
			insert into cierre values(2020, 12, 7, '2020-12-08', '2021-01-07', '2021-01-22');
			insert into cierre values(2020, 1, 8, '2020-01-09', '2020-02-08', '2020-02-23');
			insert into cierre values(2020, 2, 8, '2020-02-09', '2020-03-08', '2020-03-23');
			insert into cierre values(2020, 3, 8, '2020-03-09', '2020-04-08', '2020-04-23');
			insert into cierre values(2020, 4, 8, '2020-04-09', '2020-05-08', '2020-05-23');
			insert into cierre values(2020, 5, 8, '2020-05-09', '2020-06-08', '2020-06-23');
			insert into cierre values(2020, 6, 8, '2020-06-09', '2020-07-08', '2020-07-23');
			insert into cierre values(2020, 7, 8, '2020-07-09', '2020-08-08', '2020-08-23');
			insert into cierre values(2020, 8, 8, '2020-08-09', '2020-09-08', '2020-09-23');
			insert into cierre values(2020, 9, 8, '2020-09-09', '2020-10-08', '2020-10-23');
			insert into cierre values(2020, 10, 8, '2020-10-09', '2020-11-08', '2020-11-23');
			insert into cierre values(2020, 11, 8, '2020-11-09', '2020-12-08', '2020-12-23');
			insert into cierre values(2020, 12, 8, '2020-12-09', '2021-01-08', '2021-01-23');
			insert into cierre values(2020, 1, 9, '2020-01-10', '2020-02-09', '2020-02-24');
			insert into cierre values(2020, 2, 9, '2020-02-10', '2020-03-09', '2020-03-24');
			insert into cierre values(2020, 3, 9, '2020-03-10', '2020-04-09', '2020-04-24');
			insert into cierre values(2020, 4, 9, '2020-04-10', '2020-05-09', '2020-05-24');
			insert into cierre values(2020, 5, 9, '2020-05-10', '2020-06-09', '2020-06-24');
			insert into cierre values(2020, 6, 9, '2020-06-10', '2020-07-09', '2020-07-24');
			insert into cierre values(2020, 7, 9, '2020-07-10', '2020-08-09', '2020-08-24');
			insert into cierre values(2020, 8, 9, '2020-08-10', '2020-09-09', '2020-09-24');
			insert into cierre values(2020, 9, 9, '2020-09-10', '2020-10-09', '2020-10-24');
			insert into cierre values(2020, 10, 9, '2020-10-10', '2020-11-09', '2020-11-24');
			insert into cierre values(2020, 11, 9, '2020-11-10', '2020-12-09', '2020-12-24');
			insert into cierre values(2020, 12, 9, '2020-12-10', '2021-01-09', '2021-01-24');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('8439201934832810', '863', 3, 4000);
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('7850635239736521', '123', 17, 10750);
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('8472691728478372', '363', 2, 7000);
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('2462462543653642', '738', 7, 2500);
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('1879876542345653', '745', 2, '80');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('6734347778733455', '245', 3, '40');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('8439201934832810', '863', 3, '2000');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('8439201934832810', '863', 3, '3000');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('8439201934832810', '863', 3, '5000');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('8439201934832810', '863', 3,'2000');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('8439201934832810', '863', 3, '95000');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('2462462543653642', '645', 1, '2000');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('2462462543653642', '645', 4, '3000');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('1039482830482950', '7346', 6, '24000');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('1039482830482950', '7346', 7, '9000');
			insert into consumo(nrotarjeta, codseguridad, nrocomercio, monto) values('8439201934832810', '863', 3, '90000');`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("\n CARGA DE DATOS EXITOSA\n\n", separator, "\n")
}

func createFunction1() {
	_, err = db.Exec(`create or replace function autorizar_compra(n_tarjeta tarjeta.nrotarjeta%type,
						codigo tarjeta.codseguridad%type,
						n_comercio comercio.nrocomercio%type,
						monto_abonado compra.monto%type) returns boolean as $$
			declare
				tarjeta_encontrada record;  
				compras_pendientes_de_pago compra.monto%type;
			begin
				select * into tarjeta_encontrada from tarjeta t where n_tarjeta = t.nrotarjeta; 
				compras_pendientes_de_pago := (select sum (monto) from compra c where c.nrotarjeta = n_tarjeta and c.pagado 									= false);
				if not found  then           
					insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
						values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 'Tarjeta no valida');
					return false;	
				elsif tarjeta_encontrada.codseguridad != codigo then
					 insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
						values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 
							'Codigo de seguridad no valido');    
					 return false;
				elsif tarjeta_encontrada.estado = 'suspendida' then
					 insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
					 	values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 
							'La tarjeta se encuentra suspendida');    
					 return false;
				elsif tarjeta_encontrada.estado = 'anulada' then
					insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
						values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 
							'Plazo de vigencia expirado');    
					return false;
				elsif tarjeta_encontrada.limitecompra < (compras_pendientes_de_pago + monto_abonado)  then
					insert into rechazo (nrotarjeta, nrocomercio, fecha, monto, motivo)
						values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, 
							'Supera límite de tarjeta');    
					return false;
				else
					insert into compra (nrotarjeta, nrocomercio, fecha, monto, pagado)
						 values (n_tarjeta, n_comercio, current_timestamp, monto_abonado, false);
					return true;
				end if;
			end;
		$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("\n FUNCION autorizar_compra CREADA EXITOSA\n\n")
}

func createFunction2() {
	_, err = db.Exec(`create or replace function generar_resumen(n_cliente cliente.nrocliente%type,
								anio_par int,
								mes_par int) returns void as $$
			declare
				cliente_encontrado record;
				compra_aux record;
				tarjeta_aux record;
				cierre_aux record;
				total_aux cabecera.total%type;
				nroresumen_aux cabecera.nroresumen%type;
				nombre_comercio comercio.nombre%type;
				cont int := 1;
			begin
				select * into cliente_encontrado from cliente where nrocliente = n_cliente;
				  if not found then
			      		  raise 'Cliente % no existe.', n_cliente;
		  		  end if;
				for tarjeta_aux in select * from tarjeta where nrocliente = n_cliente loop
					total_aux := 0;
					select * into cierre_aux from cierre cie where cie.anio = anio_par and cie.mes = mes_par and 									cie.terminacion = substring(tarjeta_aux.nrotarjeta, 16, 1)::int;
					insert into cabecera(nombre, apellido, domicilio, nrotarjeta, desde, hasta, vence) 
							values (cliente_encontrado.nombre, cliente_encontrado.apellido, 								cliente_encontrado.domicilio, tarjeta_aux.nrotarjeta, 									cierre_aux.fechainicio, cierre_aux.fechacierre, cierre_aux.fechavto);
					select into nroresumen_aux nroresumen from cabecera where nrotarjeta = tarjeta_aux.nrotarjeta
											and desde = cierre_aux.fechainicio
											and hasta = cierre_aux.fechacierre;
					for compra_aux in select * from compra where nrotarjeta = tarjeta_aux.nrotarjeta 
										and fecha::date >= (cierre_aux.fechainicio)::date 
										and fecha::date <= (cierre_aux.fechacierre)::date
										and pagado = false loop
						nombre_comercio := (select nombre from comercio where nrocomercio = compra_aux.nrocomercio);
						insert into detalle values (nroresumen_aux, cont, compra_aux.fecha, nombre_comercio, 											compra_aux.monto);
						total_aux := total_aux + compra_aux.monto;
						cont := cont + 1;
						update compra set pagado = true where nrooperacion = compra_aux.nrooperacion;
					end loop;
					update cabecera set total = total_aux where nrotarjeta = tarjeta_aux.nrotarjeta
											and desde = cierre_aux.fechainicio
											and hasta = cierre_aux.fechacierre;
				end loop;
			end;
		$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("\n FUNCION generar_resumen CREADA EXITOSAMENTE\n\n")
}

func createFunction3() {
	_, err = db.Exec(`create or replace function alertar_rechazos() returns trigger as $$
			begin
				insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
					values (new.nrotarjeta, current_timestamp, new.nrorechazo, 0, 
						concat('Se ha rechazado su compra porque "', new.motivo, '"'));
				perform * from rechazo r where r.nrotarjeta = new.nrotarjeta
								and r.nrorechazo != new.nrorechazo
								and extract(day from r.fecha) = extract(day from new.fecha)
								and extract(month from r.fecha) = extract(month from new.fecha)
								and extract(year from r.fecha) = extract(year from new.fecha)
								and r.motivo = 'Supera límite de tarjeta'
								and new.motivo = 'Supera límite de tarjeta';
				if found then
					update tarjeta set estado = 'suspendida' where nrotarjeta = new.nrotarjeta;
					insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
						values (new.nrotarjeta, current_timestamp, new.nrorechazo, 32, 
							'Se ha suspendido la tarjeta preventivamente');
				end if;
				return new;
			end;
		$$ language plpgsql;
		create or replace function alertar_compras() returns trigger as $$
			begin  
				perform * from compra c where c.nrotarjeta = new.nrotarjeta
								and c.nrooperacion != new.nrooperacion
								and c.fecha >= new.fecha - (1 * interval '1 minute')
								and c.nrocomercio != new.nrocomercio
								and (select codigopostal from comercio where nrocomercio = c.nrocomercio) = 									(select codigopostal from comercio where nrocomercio = new.nrocomercio);
				if found then
					insert into alerta (nrotarjeta, fecha, nrooperacion, codalerta, descripcion)
						values (new.nrotarjeta, current_timestamp, new.nrooperacion, 1, 
							'Se han realizado dos compras al mismo tiempo en comercios diferentes');
				end if;
				perform * from compra c where c.nrotarjeta = new.nrotarjeta
								and c.nrooperacion != new.nrooperacion
								and c.fecha >= new.fecha - (5 * interval '1 minute')
								and (select codigopostal from comercio where nrocomercio = 							c.nrocomercio) != (select codigopostal from comercio where nrocomercio = new.nrocomercio);
				if found then
					insert into alerta (nrotarjeta, fecha, nrooperacion, codalerta, descripcion)
						values (new.nrotarjeta, current_timestamp, new.nrooperacion, 5, 
							'Se han realizado dos compras seguidas en comercios muy alejados');
				end if;
				return new;
			end;
		$$ language plpgsql;
		create trigger rechazos_tgr after insert on rechazo for each row execute procedure alertar_rechazos();
		create trigger compras_tgr after insert on compra for each row execute procedure alertar_compras();`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("\n FUNCIONES alertar_rechazo Y alertar_compra CREADAS EXITOSAMENTE\n\n", separator, "\n")
}

func autorizarCompra() {
	if nroConsumo <= cantConsumos {
		row, err := db.Query(`select * from consumo where nroconsumo = $1;`, nroConsumo)
		if err != nil {
			log.Fatal(err)
		}
		defer row.Close()
		var c consumo
		var r bool
		if row.Next() {
			if err = row.Scan(&c.nroconsumo, &c.nrotarjeta, &c.codseguridad, &c.nrocomercio, &c.monto); err != nil {
				log.Fatal(err)
			}
		}
		row, err = db.Query(`select autorizar_compra($1::char(16), $2::char(4), $3::int, $4::decimal(7,2));`, c.nrotarjeta, c.codseguridad, c.nrocomercio, c.monto)
		if err != nil {
			log.Fatal(err)
		}
		if row.Next() {
			if err = row.Scan(&r); err != nil {
				log.Fatal(err)
			}
		}
		if err != nil {
			log.Fatal(err)
		}
		if r {
			fmt.Print("\nCOMPRA ", nroConsumo, " AUTORIZADA\n\n", separator, "\n")
		} else {
			fmt.Print("\nCOMPRA ", nroConsumo, " RECHAZADA\n\n", separator, "\n")
		}
		nroConsumo = nroConsumo + 1
	} else {
		fmt.Print("\n NO HAY MAS COMPRAS POR AUTORIZAR\n\n", separator, "\n")
	}
}

func generarResumenes() {
	_, err = db.Query(`select generar_resumen(1, 2020, 5);
				select generar_resumen(4, 2020, 5);
				select generar_resumen(12, 2020, 5);
				select generar_resumen(19, 2020, 5);
				select generar_resumen(20, 2020, 5);
				select generar_resumen(3, 2020, 5);
				select generar_resumen(1, 2020, 6);
				select generar_resumen(4, 2020, 6);
				select generar_resumen(12, 2020, 6);
				select generar_resumen(19, 2020, 6);
				select generar_resumen(20, 2020, 6);
				select generar_resumen(3, 2020, 6);`)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print("\n RESUMENES GENERADOS EXITOSAMENTE\n\n", separator, "\n")
}

func detectarAlertas() {
	for opcion != 9 {
		var alertasAux int = 0
		proximaVez := time.Now().Truncate(time.Minute)
		proximaVez = proximaVez.Add(time.Minute)
		row, err := db.Query(`select count(*) from alerta;`)
		if err != nil {
			log.Fatal(err)
		}
		if row.Next() {
			if err = row.Scan(&alertasAux); err != nil {
				log.Fatal(err)
			}
		}
		if err != nil {
			log.Fatal(err)
		}
		if alertas < alertasAux {
			alertas = alertasAux
			fmt.Print("\n\n", separator, "\n\n SE HA DETECTADO UNA NUEVA ALERTA\n\n", separator, "\n\n")
		}
		time.Sleep(60 * time.Second)
	}
}
